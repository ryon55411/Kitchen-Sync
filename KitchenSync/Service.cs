﻿using Dalamud.IoC;
using Dalamud.Plugin;
using Dalamud.Plugin.Services;
using KitchenSync.Data;
using KitchenSync.System;

namespace KitchenSync;

internal class Service
{
    [PluginService] public static DalamudPluginInterface PluginInterface { get; private set; } = null!;
    [PluginService] public static IClientState ClientState { get; private set; } = null!;
    [PluginService] public static IGameGui GameGui { get; private set; } = null!;
    [PluginService] public static ITextureProvider Texture { get; private set; } = null!;
    [PluginService] public static IGameInteropProvider Hooks { get; private set; } = null!;

    public static Configuration Configuration = null!;
    public static HotbarManager HotbarManager = null!;
}